# Ejercicio: Aplicación de Gestión de Tareas

## Descripción

Desarrolla una aplicación simple de gestión de tareas. La aplicación debe permitir a los usuarios crear, editar y eliminar tareas.

## Requisitos

1. Página Principal (Index):
    - Mostrar todas las tareas.
    - Cada tarea debe tener un título, una descripción y un botón para eliminarla.

2. Creación de Tareas:
    -Agregar un botón "Agregar Tarea" que abre un formulario modal.
    - El formulario debe tener campos para el título y la descripción de la tarea.
    - Al agregar una tarea, esta debe ser mostrada en la lista.

3. Edición de Tareas:
    - Al hacer clic en una tarea, permitir la edición del título y la descripción en un formulario modal.
    - Al guardar los cambios, la tarea debe actualizarse en la lista.

4. Persistencia de Datos:
    - Al recargar la página, las tareas deben persistir.

5. Lista de tareas:
    - La lista de tareas se debe poder ordenar.
    - Se valorará la forma de hacerlo tanto a nivel de usabilidad como a nivel de código.

> **Notas:** no utilices los estilos de ejemplo de Visual Studio u otro IDE, puedes usar Bootstrap pero crea tu propio layout.
